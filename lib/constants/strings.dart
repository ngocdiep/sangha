class Strings {
  static const String DASHBOARD_HOME = "Home";
  static const String DASHBOARD_MAP = "Map";
  static const String DASHBOARD_PROFILE = "Profile";
  static const String DASHBOARD_SETTING = "Settings";
}
