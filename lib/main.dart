import 'package:flutter/material.dart';
import 'package:sangha/ui/dashboard/dashboard.dart';

void main() => runApp(SanghaApp());

class SanghaApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sangha',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Dashboard(title: 'Sangha Dashboard'),
    );
  }
}
