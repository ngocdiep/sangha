import 'package:flutter/material.dart';
import 'package:sangha/ui/dashboard/adapter.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final BottomNavigationBarAdapter pageAdapter = BottomNavigationBarAdapter();

  void onTabTapped(int index) {
    setState(() {
      pageAdapter.currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: pageAdapter.getBody(),
      bottomNavigationBar: BottomNavigationBar(
        onTap: onTabTapped,
        currentIndex: pageAdapter.currentIndex,
        items: pageAdapter.buildBottomNavigationBarItemList(),
      ),
    );
  }
}
