import 'package:flutter/material.dart';
import 'package:sangha/constants/strings.dart';
import 'package:sangha/ui/dashboard/tabs/sangha_map.dart';

class BottomNavigationBarContainer {
  final IconData icon;
  final String title;
  final Widget body;

  BottomNavigationBarContainer(this.icon, this.title, this.body);

  buildBottomNavigationBarItem() => BottomNavigationBarItem(
      icon: Icon(this.icon),
      title: Text(this.title),
      backgroundColor: Colors.blue);
}

class BottomNavigationBarAdapter {
  List<BottomNavigationBarContainer> _stack;
  int currentIndex;

  BottomNavigationBarAdapter() {
    this._stack = createBottomNavigationBarItemList();
    this.currentIndex = 0;
  }

  createBottomNavigationBarItemList() => [
        BottomNavigationBarContainer(Icons.home, Strings.DASHBOARD_HOME,
            PlaceholderWidget(Colors.white)),
        BottomNavigationBarContainer(
            Icons.map, Strings.DASHBOARD_MAP, SanghaMap(Strings.DASHBOARD_MAP)),
        BottomNavigationBarContainer(Icons.account_circle,
            Strings.DASHBOARD_PROFILE, PlaceholderWidget(Colors.green)),
        BottomNavigationBarContainer(Icons.settings, Strings.DASHBOARD_SETTING,
            PlaceholderWidget(Colors.redAccent)),
      ];

  getBody() => _stack[currentIndex].body;

  buildBottomNavigationBarItemList() {
    List<BottomNavigationBarItem> itemList = new List();
    for (BottomNavigationBarContainer item in _stack) {
      itemList.add(item.buildBottomNavigationBarItem());
    }
    return itemList;
  }
}

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: color,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SanghaMap(Strings.DASHBOARD_MAP)),
            ),
        label: Text('Open map'),
        icon: Icon(Icons.directions_boat),
      ),
    );
  }
}
