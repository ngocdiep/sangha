// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:sangha/main.dart';

void main() {
  testWidgets('First load is Home page', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(SanghaApp());

//    // Verify that Bottom page is loaded
//    expect(find.text('Home'), findsOneWidget);
//    expect(find.text('Map'), findsOneWidget);
//    expect(find.text('Profile'), findsOneWidget);
//    expect(find.text('Settings'), findsOneWidget);
//
//    // Tap the map icon and trigger a frame.
//    await tester.tap(find.byIcon(Icons.map));
//    await tester.pump();
//
//    // Verify that Map page is loaded
//    expect(find.text('Home'), findsOneWidget);
//    expect(find.text('Map'), findsOneWidget);
  });
}
